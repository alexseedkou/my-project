def scmVars

properties([[$class: 'GitLabConnectionProperty', gitLabConnection: 'gitlab']])

gitlabBuilds(builds:[ 'Checkout', 'Dependency install', 'Lint', 'Build', 'Deploy']) {
    
}

node {
    try {            


        stage('Checkout') {
            gitlabCommitStatus(name: 'Checkout') {
                scmVars = checkout scm;
                notifyBuild('STARTED', scmVars.GIT_COMMIT)
            }
        }

        docker.image('node:8-alpine').inside {

            withEnv([
                'HOME=.'
            ]) {

                stage('Dependency install') {
                    try {
                        gitlabCommitStatus(name: 'Lint') {
                            sh 'npm ci'
                        }
                    } catch(e) {
                        throw e
                        updateGitlabCommitStatus(name: 'Dependency install', state: 'failed')
                    }
                }

                stage('Lint') {
                    gitlabCommitStatus(name: 'Lint') {
                        sh 'npm run lint'
                    }
                }

                stage('Build') {
                    gitlabCommitStatus(name: 'Build') {
                        sh 'npm run build'
                    }
                }

                stage('Deploy') {
                    sh 'rm -r node_modules/'
                    sh 'pwd'
                    gitlabCommitStatus(name: 'Deploy') {
                        script {
                            sshPublisher(publishers: [sshPublisherDesc(configName: 'frontend server', transfers: [sshTransfer(cleanRemote: false, excludes: '', execCommand: '', execTimeout: 120000, flatten: false, makeEmptyDirs: false, noDefaultExcludes: false, patternSeparator: '[, ]+', remoteDirectory: 'monaFE', remoteDirectorySDF: false, removePrefix: '', sourceFiles: 'dist/**/*')], usePromotionTimestamp: false, useWorkspaceInPromotion: false, verbose: true)])
                        }
                    }
                }
            }
        }

    } catch (e) {
        currentBuild.result = "FAILED"
        throw e
    } finally {
        notifyBuild(currentBuild.result, scmVars.GIT_COMMIT)
    }
}

def notifyBuild(String buildStatus = 'STARTED', String commit) {
    // build status of null means successful
    buildStatus =  buildStatus ?: 'SUCCESS'
    // Default values
    def colorName = 'RED'
    def colorCode = '#FF0000'
    def buildUser = getBuildUser()
    if (buildUser == null) buildUser = sh(returnStdout: true, script: "git --no-pager show -s --format='%an' ${commit}" )
    def subject = "@channel ${buildStatus}: Job '${env.JOB_NAME} build [${env.BUILD_NUMBER}] by ${buildUser}\n'"
    def summary = "${subject} More info at: (${env.BUILD_URL})"

    // Override default values based on build status
    if (buildStatus == 'STARTED') {
        color = 'YELLOW'
        colorCode = '#FFFF00'
    } else if (buildStatus == 'FAILURE') {
        color = 'RED'
        colorCode = '#FF0000'
    } else {
        color = 'GREEN'
        colorCode = '#00FF00'
    }

    // Send notifications
    echo buildStatus
    slackSend (channel: '#deployment', color: colorCode, message: summary)
}   
    


def getBuildUser() {
    def currentJob = currentBuild.rawBuild.getCause(Cause.UserIdCause);
    if (currentJob == null) return null;
    return currentJob.getUserId();
}
