import axios from 'axios'

// 克隆一个axios
const request = axios.create({
  baseURL: ''
})

// 添加一个请求拦截器
request.interceptors.request.use(function (config) {
  // 请求提交之前要做的
  return config
}, function (error) {
  // 请求错误
  return Promise.reject(error)
})

// 添加一个响应拦截器
request.interceptors.response.use(function (response) {
  // 如果响应结果对象中有 data，则直接返回这个 data 数据
  // 如果响应结果对象中没有 data，则不作任何处理，直接原样返回这个数据
  return response.data.data || response.data
}, function (error) {
  // Do something with response error
  return Promise.reject(error)
})

export default request
