// server.js
var express = require('express')
// var path = require('path');
var serveStatic = require('serve-static')
var app = express()
app.use(serveStatic('/dist'))
// var port = process.env.PORT || 8888;
var port = 8888
var hostname = 'ec2-52-83-48-36.cn-northwest-1.compute.amazonaws.com.cn'

app.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`)
})
